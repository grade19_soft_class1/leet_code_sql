create  table  remove
(
	remove_id		int 	primary   key,
	remove_num	int	not null,
	remove_name	nvarchar(50)	
)



create  table  teacher
(
	teacher_id		int 	primary  key,
	teacher_name	nvarchar(50)	not  null
)



create  table  remove_teacher
(
	remove_id		int	constraint		fk_remove_id	foreign  key   references   remove(remove_id),
	teacher_id		int	constraint		fk_teacher_id	foreign  key   references   teacher(teacher_id),
	course		nvarchar(50),
	constraint		pk_remove_id_teacher_id	primary  key(remove_id,teacher_id)
)
