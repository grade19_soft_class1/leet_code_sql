Create Table World
(
Id  int primary key identity(1,1)  not null,
Name varchar(255) not null,
Continent	varchar(255) not null,
area int  not null,
[population] int not null,
gdp int not null,
)

insert into World(Name,Continent,area,[population],gdp) values ('Afghanistan','Asia',652230,25500100,20343000  ),
('Albania','Europe',28748, 2831741,12960000),('Algeria','Africa', 2381741,37100000,188681000),
('Andorra','Europe',468,78115,3712000),('Angola','Africa',1246700,20609294,100990000)

select l1.Name,l1.[population],l1.area from World l1 where [population]>25000000 or area>3000000

