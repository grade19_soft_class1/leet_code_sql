编写一个 SQL 查询，查找所有至少连续出现三次的数字。
+----+-----+
| Id | Num |
+----+-----+
| 1  |  1  |
| 2  |  1  |
| 3  |  1  |
| 4  |  2  |
| 5  |  1  |
| 6  |  2  |
| 7  |  2  |
+----+-----+
例如，给定上面的 Logs 表， 1 是唯一连续出现至少三次的数字。
+-----------------+
| ConsecutiveNums |
+-----------------+
| 1               |
+-----------------+



WITH logs_cte(lastId, curId, Num, NumCount) AS
(
    SELECT TOP 1 Id, Id+1, Num, 1 FROM Logs
    UNION ALL
    SELECT l.Id, l.Id+1, l.Num, (CASE 
		WHEN l.Num=c.Num THEN c.NumCount+1
		ELSE 1 END) FROM Logs l
    INNER JOIN logs_cte c ON l.Id=c.curId
)
SELECT Num ConsecutiveNums  FROM logs_cte
WHERE NumCount=3