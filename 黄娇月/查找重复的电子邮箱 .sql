--查找重复的电子邮箱 
use Marr
go
create table person(id int identity not null,email varchar(10))
go
set nocount on
insert into person values('a@b.com')
insert into person values('c@d.com')
insert into person values('a@b.com')

select email from person 
where email in(select email from person group by email having count(1)>1)
group by email