use Marr
go
if exists(select * from sysobjects where name='cinema')
drop table cinema
go
create table cinema
(
	id int identity not null,
	movie varchar(50),
	description varchar(50),
	rating float
)
go
set nocount on
insert into cinema values('war','great 3D',8.9)
insert into cinema values('science','fiction',8.5)
insert into cinema values('irish','boring ',6.2)
insert into cinema values('ice song','fantacy',8.6)
insert into cinema values('house card','interesting',9.1)



select * from cinema
where rating>=8.9
order by rating desc