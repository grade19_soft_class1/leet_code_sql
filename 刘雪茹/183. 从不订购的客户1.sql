/*183. 从不订购的客户
某网站包含两个表，Customers 表和 Orders 表。编写一个 SQL 查询，找出所有从不订购任何东西的客户。
Customers 表：         
+----+-------+
| Id | Name  |
+----+-------+
| 1  | Joe   |
| 2  | Henry |
| 3  | Sam   |
| 4  | Max   |
+----+-------+
Orders 表：
+----+------------+
| Id | CustomerId |
+----+------------+
| 1  | 3          |
| 2  | 1          |
+----+------------+
例如给定上述表格，你的查询应返回：
+-----------+
| Customers |
+-----------+
| Henry     |
| Max       |
+-----------+
*/
create table  Customers 
(
  Id int primary key identity(1,1),
  Name nvarchar(50) 
)
create table Orders
(
  Id int primary key identity(1,1),
  CustomerId int references Customers(Id) not null
)
/*select *  from  Orders
left join Customers on Customers.Id=Orders.CustomerId 
where Orders.Id is null*/

--法一
select c.name as customers
from dbo.customers as c
    left join dbo.orders as o
        on c.id = o.customerid
where o.id is null
--法二
select c.name as customers
from dbo.customers as c
where not exists
(
    select customerid from dbo.orders where c.id = customerid
)

