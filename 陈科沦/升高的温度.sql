Create table Weather (Id int, RecordDate date, Temperature int)

insert into Weather (Id, RecordDate, Temperature) values ('1', '2015-01-01', '40')
insert into Weather (Id, RecordDate, Temperature) values ('2', '2015-01-02', '25')
insert into Weather (Id, RecordDate, Temperature) values ('3', '2015-01-03', '10')
insert into Weather (Id, RecordDate, Temperature) values ('4', '2015-01-04', '30')
SELECT
W1.Id

from Weather	As W1,Weather As W2

where W1.RecordDate = DATEADD (Day,1,W2.RecordDate)

and W1.Temperature > W2.Temperature

drop table Weather	



