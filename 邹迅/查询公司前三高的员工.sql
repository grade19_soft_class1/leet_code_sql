
Create database Test1

Create table Employee
(
 Id int primary key identity(1,1) not null,
 Name varchar(255) not null,
 Salary  int not null,
 Department int not null,
)
Create	table Department 
(
Id int primary key identity(1,1) not null,
Name varchar(255) not null,
)

insert into Employee(Name,Salary,Department)  values ('Joe','85000',1),('Henry',80000,2),('Sam',60000,2),('Max',90000,1),
 ('Janet',69000,1),('Randy',85000,1),('Will',70000,1)

 insert into Department(Name)  values('IT') ,('Sales')	

select e1.Name as 'Employee',l1.Name as Department , e1.Salary
from Employee e1  
join Department l1 on l1.Id=e1.Department
where 3 >
(
    select count(distinct e2.Salary)
    from Employee e2
    where e2.Salary > e1.Salary
	and e1.Department=e2.Department
)
