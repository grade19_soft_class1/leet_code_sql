/*
给定一个 Weather 表，编写一个 SQL 查询，来查找与之前（昨天的）日期相比温度更高的所有日期的 Id。

+---------+------------------+------------------+
| Id(INT) | RecordDate(DATE) | Temperature(INT) |
+---------+------------------+------------------+
|       1 |       2015-01-01 |               10 |
|       2 |       2015-01-02 |               25 |
|       3 |       2015-01-03 |               20 |
|       4 |       2015-01-04 |               30 |
+---------+------------------+------------------+
例如，根据上述给定的 Weather 表格，返回如下 Id:

+----+
| Id |
+----+
|  2 |
|  4 |
+----+
*/

create table Weather
(
	Id Int Primary Key identity,
	RecordDate date,
	Temperature int
)


insert into Weather(RecordDate,Temperature) values('2015-01-01',10);
insert into Weather(RecordDate,Temperature) values('2015-01-02',25);
insert into Weather(RecordDate,Temperature) values('2015-01-03',20);
insert into Weather(RecordDate,Temperature) values('2015-01-04',30);


--解决方案

select id from Weather