/*
某网站包含两个表，Customers 表和 Orders 表。编写一个 SQL 查询，找出所有从不订购任何东西的客户。

Customers 表：

+----+-------+
| Id | Name  |
+----+-------+
| 1  | Joe   |
| 2  | Henry |
| 3  | Sam   |
| 4  | Max   |
+----+-------+
Orders 表：

+----+------------+
| Id | CustomerId |
+----+------------+
| 1  | 3          |
| 2  | 1          |
+----+------------+
例如给定上述表格，你的查询应返回：

+-----------+
| Customers |
+-----------+
| Henry     |
| Max       |
+-----------+
*/


create table Customers
(
	Id int primary key identity,
	Name nvarchar(255)
)

create table Orders
(
	Id int primary key identity,
	CustomerId int
)

insert into Customers (Name) values ('Joe')
insert into Customers (Name) values ('Henry')
insert into Customers (Name) values ('Sam')
insert into Customers (Name) values ('Max')

insert into Orders (CustomerId) values ('3')
insert into Orders (CustomerId) values ('1')


--解决方案
select name as Customers from Customers  full join Orders on Customers.Id=Orders.CustomerId where Orders.CustomerId is null; --方法一

select Customers.name as 'Customers'
from customers
where customers.id not in
(
	select customerid from orders
);--方法二