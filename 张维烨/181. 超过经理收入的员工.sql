﻿/*
Employee 表包含所有员工，他们的经理也属于员工。每个员工都有一个 Id，此外还有一列对应员工的经理的 Id。

+----+-------+--------+-----------+
| Id | Name  | Salary | ManagerId |
+----+-------+--------+-----------+
| 1  | Joe   | 70000  | 3         |
| 2  | Henry | 80000  | 4         |
| 3  | Sam   | 60000  | NULL      |
| 4  | Max   | 90000  | NULL      |
+----+-------+--------+-----------+
给定 Employee 表，编写一个 SQL 查询，该查询可以获取收入超过他们经理的员工的姓名。在上面的表格中，Joe 是唯一一个收入超过他的经理的员工。

+----------+
| Employee |
+----------+
| Joe      |
+----------+
*/

create table Employee181
(
	Id int primary key identity,
	Name nvarchar(80),
	Salary int,
	ManagerId int
)

insert into Employee181(Name,Salary,ManagerId)
select 'Joe',70000,3 union
select 'Henry',80000,4 union
select 'Sam',60000,null union
select 'Max',90000,null;


select * from Employee181;


select Name as Employee from Employee181 where ManagerId>Id  order by Salary desc 
offset 0 rows fetch next 1 rows only;--未完成