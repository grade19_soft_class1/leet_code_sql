﻿/*
编写一个 SQL 查询，查找 Person 表中所有重复的电子邮箱。

示例：

+----+---------+
| Id | Email   |
+----+---------+
| 1  | a@b.com |
| 2  | c@d.com |
| 3  | a@b.com |
+----+---------+
根据以上输入，你的查询应返回以下结果：

+---------+
| Email   |
+---------+
| a@b.com |
+---------+

*/
Create table Person1
(
Id  int primary key identity(1,1) not null,
Email varchar(255) not null,
)

insert into Person1(Email) values('a@b.com'),('c@d.com'),('a@b.com')
select Email from Person1

select  distinct l1.Email from Person1 l1, Person1 l2 where l1.Email=l2.Email and l1.Id!=l2.Id