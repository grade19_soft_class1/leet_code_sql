﻿/*
Employee 表包含所有员工，他们的经理也属于员工。每个员工都有一个 Id，此外还有一列对应员工的经理的 Id。

+----+-------+--------+-----------+
| Id | Name  | Salary | ManagerId |
+----+-------+--------+-----------+
| 1  | Joe   | 70000  | 3         |
| 2  | Henry | 80000  | 4         |
| 3  | Sam   | 60000  | NULL      |
| 4  | Max   | 90000  | NULL      |
+----+-------+--------+-----------+
给定 Employee 表，编写一个 SQL 查询，该查询可以获取收入超过他们经理的员工的姓名。在上面的表格中，Joe 是唯一一个收入超过他的经理的员工。

+----------+
| Employee |
+----------+
| Joe      |
+----------+

*/
Create table Employee1 
(
Id int primary key identity(1,1) not null,
Name varchar(255) not null,
Salary int not null,
ManagerId  int  null,
)

insert into Employee1(Name,Salary,ManagerId) values('joe',700,3),('Henry',800,4),('Sam',600,Null),('Max',900,Null)


select Name as Employee1  from Employee1 P where   Salary> (select Salary from Employee1 A where A.Id=P.ManagerId)
