create table Person
(
	PersonId int primary key not null identity,
	FirstName varchar not null,
	LastName varchar not null
)
go

create table Address
(
	AddressId int primary key not null identity,
	PersonId int not null,
	City varchar not null,
	State varchar not null
)
go
select*from Person
select * from Address

select Person.FirstName,Person.LastName,Address.City,Address.State from Person left join Address 
on Person.PersonId = Address.PersonId