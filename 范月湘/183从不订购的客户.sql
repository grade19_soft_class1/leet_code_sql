USE [Leetcode]
create table Customers
(
	Id int primary key not null identity,
	Name int not null 
)
go
insert into Customers(Name)values('Joe'),('Henry'),('Sam'),('Max')
create table Orders
(
	Id int primary key not null identity,
	CustomerId int not null
)
go
insert into Orders(CustomerId)values(3),(1)

select CustomerId from Orders

select*from Customers a
left join Orders b
on a.Id=b.CustomerId

select Customers.Name as 'Customers'
from Customers
where Customers.Id not in
(
	select CustomerId from Orders
)

select a.Name from Customers a left join Orders b
on a.Id=b.CustomerId
where b.CustomerId is null

 