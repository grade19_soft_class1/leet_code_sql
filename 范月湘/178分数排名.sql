create table Scores
(
	id int primary key not null identity,
	Score float not null

)
go

select * from Scores

insert into Scores(Score)values('3.50')
insert into Scores(Score)values('3.65')
insert into Scores(Score)values('4.0')
insert into Scores(Score)values('3.85')
insert into Scores(Score)values('4.0')
insert into Scores(Score)values('3.65')
select a.Score , 
(select count(distinct b.Score) from Scores b where a.Score <= b.Score ) as Rank  
from Scores a
order by Score desc


 