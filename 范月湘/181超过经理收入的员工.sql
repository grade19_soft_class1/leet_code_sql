
create table Employee
(
	Id int primary key not null identity,
	Name varchar(10) not null,
	Salary int null,
	ManagerId int null
)
go
insert into Employee(Name,Salary,ManagerId) values('Joe',70000,3),('Henry',80000,4),('Sam',60000,null),('Max',90000,null)

drop table Employee

create table Moon
(
	Id int primary key not null identity,
	Name varchar not null,
	Salary int not null
)
create table Star
(
	Id int primary key not null identity,
	Name varchar not null,
	Salary int not null
)


select a.Name from Employee a,Employee b
where a.ManagerId =b.Id and a.Salary > b.Salary

