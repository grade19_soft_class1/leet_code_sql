/*某网站包含两个表，Customers 表和 Orders 表。编写一个 SQL 查询，找出所有从不订购任何东西的客户。

Customers 表：

+----+-------+
| Id | Name  |
+----+-------+
| 1  | Joe   |
| 2  | Henry |
| 3  | Sam   |
| 4  | Max   |
+----+-------+
Orders 表：

+----+------------+
| Id | CustomerId |
+----+------------+
| 1  | 3          |
| 2  | 1          |
+----+------------+
例如给定上述表格，你的查询应返回：

+-----------+
| Customers |
+-----------+
| Henry     |
| Max       |
+-----------+*/

create table Customers
(
Id int identity primary key,
Name nvarchar(50) not null
)

insert into Customers(Name) values('Joe')
insert into Customers(Name) values('Henry')
insert into Customers(Name) values('Sam')
insert into Customers(Name) values('Max')



create table Orders
(
Id int identity primary key,
CustomerId int not null
)

insert into Orders(CustomerId) values('3')
insert into Orders(CustomerId) values('1')

select * from Customers

select * from Orders


select Customers.Name Customers from Customers left join Orders on Orders.CustomerId = Customers.Id where Orders.Id is null
