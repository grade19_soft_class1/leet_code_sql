﻿/*Employee 表包含所有员工，他们的经理也属于员工。每个员工都有一个 Id，此外还有一列对应员工的经理的 Id。

+----+-------+--------+-----------+
| Id | Name  | Salary | ManagerId |
+----+-------+--------+-----------+
| 1  | Joe   | 70000  | 3         |
| 2  | Henry | 80000  | 4         |
| 3  | Sam   | 60000  | NULL      |
| 4  | Max   | 90000  | NULL      |
+----+-------+--------+-----------+
给定 Employee 表，编写一个 SQL 查询，该查询可以获取收入超过他们经理的员工的姓名。在上面的表格中，Joe 是唯一一个收入超过他的经理的员工。

+----------+
| Employee |
+----------+
| Joe      |
+----------+*/

create table Employee3
(
Id int identity primary key,
Name nvarchar(50) not null,
Salary int not null,
ManagerId int 
)

create table Test2
(
Id int identity primary key,
Name nvarchar(50) not null,
Salary int not null
)

insert into Test2(Name,Salary)values('Sam','60000')
insert into Test2(Name,Salary)values('Max','90000')
select * from Test2

insert into Employee3(Name,Salary,ManagerId)values('Joe','70000','3')
insert into Employee3(Name,Salary,ManagerId)values('Henry','80000','4')
insert into Employee3(Name,Salary,ManagerId)values('Sam','60000','')
insert into Employee3(Name,Salary,ManagerId)values('Max','90000','')

update Employee3 set ManagerId = null where Id = 3
update Employee3 set ManagerId = null where Id = 4

select Employee3.Name Employee from Employee3 inner join Test2 on Employee3.Id = Test2.Id and Employee3.Salary > Test2.Salary