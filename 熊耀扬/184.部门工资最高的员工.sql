﻿/*Employee 表包含所有员工信息，每个员工有其对应的 Id, salary 和 department Id。

+----+-------+--------+--------------+
| Id | Name  | Salary | DepartmentId |
+----+-------+--------+--------------+
| 1  | Joe   | 70000  | 1            |
| 2  | Henry | 80000  | 2            |
| 3  | Sam   | 60000  | 2            |
| 4  | Max   | 90000  | 1            |
+----+-------+--------+--------------+
Department 表包含公司所有部门的信息。

+----+----------+
| Id | Name     |
+----+----------+
| 1  | IT       |
| 2  | Sales    |
+----+----------+
编写一个 SQL 查询，找出每个部门工资最高的员工。例如，根据上述给定的表格，Max 在 IT 部门有最高工资，Henry 在 Sales 部门有最高工资。

+------------+----------+--------+
| Department | Employee | Salary |
+------------+----------+--------+
| IT         | Max      | 90000  |
| Sales      | Henry    | 80000  |
+------------+----------+--------+*/

create table Employee
(
Id int primary key identity,
Name nvarchar(50) not null,
Salary int not null,
DepartmentId int not null
)

create table Department
(
Id int identity primary key,
Name nvarchar(50) not null
)

select * from Employee
select * from Department


insert into Employee(Name,Salary,DepartmentId)values('Joe','70000','1')
insert into Employee(Name,Salary,DepartmentId)values('Henry','80000','2')
insert into Employee(Name,Salary,DepartmentId)values('Sam','60000','2')
insert into Employee(Name,Salary,DepartmentId)values('Max','90000','1')



insert into Department(Name)Values('IT')
insert into Department(Name)Values('Sales')

select Department.Name Department,Employee.Name Employee,Employee.Salary from Employee inner join Department on Employee.DepartmentId = Department.Id where Employee.Salary>=80000 order by Employee.Salary desc