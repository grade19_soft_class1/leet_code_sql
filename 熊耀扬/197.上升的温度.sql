﻿/*给定一个 Weather 表，编写一个 SQL 查询，来查找与之前（昨天的）日期相比温度更高的所有日期的 Id。

+---------+------------------+------------------+
| Id(INT) | RecordDate(DATE) | Temperature(INT) |
+---------+------------------+------------------+
|       1 |       2015-01-01 |               10 |
|       2 |       2015-01-02 |               25 |
|       3 |       2015-01-03 |               20 |
|       4 |       2015-01-04 |               30 |
+---------+------------------+------------------+
例如，根据上述给定的 Weather 表格，返回如下 Id:

+----+
| Id |
+----+
|  2 |
|  4 |
+----+*/

create table Weather
(
Id int identity primary key,
RecordDate DATE not null,
Temperature int  not null
)


create table Weather2
(
Id int identity primary key,
RecordDate2 DATE not null,
Temperature2 int  not null
)


select * from Weather
select * from Weather2

insert into Weather(RecordDate,Temperature)values('2015-01-01','10')
insert into Weather(RecordDate,Temperature)values('2015-01-02','25')
insert into Weather(RecordDate,Temperature)values('2015-01-03','20')
insert into Weather(RecordDate,Temperature)values('2015-01-04','30')


insert into Weather2(RecordDate2,Temperature2)values('2015-01-01','10')
insert into Weather2(RecordDate2,Temperature2)values('2015-01-02','25')
insert into Weather2(RecordDate2,Temperature2)values('2015-01-03','20')
insert into Weather2(RecordDate2,Temperature2)values('2015-01-04','30')


select Weather2.Id from Weather,Weather2 where Weather.Temperature < Weather2.Temperature2 and datediff(day,Weather2.RecordDate2,Weather.RecordDate)=1

