﻿/*编写一个 SQL 查询，获取 Employee 表中第二高的薪水（Salary） 。

+----+--------+
| Id | Salary |
+----+--------+
| 1  | 100    |
| 2  | 200    |
| 3  | 300    |
+----+--------+
例如上述 Employee 表，SQL查询应该返回 200 作为第二高的薪水。如果不存在第二高的薪水，那么查询应返回 null。

+---------------------+
| SecondHighestSalary |
+---------------------+
| 200                 |
+---------------------+*/

create table Employee
(
Id int primary key identity,
Salary int not null
)

insert into Employee(Salary) values ('100')
insert into Employee(Salary) values ('200')
insert into Employee(Salary) values ('300')

select * from Employee

select min(Salary) SecondHighestSalary from Employee where Salary >(select min(Salary) from Employee)