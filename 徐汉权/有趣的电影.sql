/*
create table Cinema(
Id int identity(1,1) not null primary key,
Movie nvarchar(30) not null ,
Description nvarchar(30) not null,
Rating nvarchar(10) not null,
)
insert into Cinema(Movie,Description,Rating) values ('War','great 3D','8.9')
insert into Cinema(Movie,Description,Rating) values ('Science','fiction','8.5')
insert into Cinema(Movie,Description,Rating) values ('irish','boring','6.2')
insert into Cinema(Movie,Description,Rating) values ('Ice song','Fantacy','8.6')
insert into Cinema(Movie,Description,Rating) values ('House card','Interesting','9.1')
*/
select * from Cinema
where Description !='boring' and Id%2=1
order by Rating desc