表1: Person

+-------------+---------+
| 列名         | 类型     |
+-------------+---------+
| PersonId    | int     |
| FirstName   | varchar |
| LastName    | varchar |
+-------------+---------+
PersonId 是上表主键
表2: Address

+-------------+---------+
| 列名         | 类型    |
+-------------+---------+
| AddressId   | int     |
| PersonId    | int     |
| City        | varchar |
| State       | varchar |
+-------------+---------+
AddressId 是上表主键
 

编写一个 SQL 查询，满足条件：无论 person 是否有地址信息，都需要基于上述两表提供 person 的以下信息：

 

FirstName, LastName, City, State



create database databasezuhelianggebiao
create table person
(
PersonId int primary key identity(1,1) not null,
FirstName varchar not null,
LastName varchar not null
)
create table Address
(AddressId int primary key identity(1,1)not null,
PersonId  int not null,
 City varchar not null,
 State varchar not null
)

select *from Person a left outer join Address b on a.PersonId=b.PersonId
