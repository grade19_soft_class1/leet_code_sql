Create table Person
(
Id  int primary key identity(1,1) not null,
Email varchar(255) not null,
)

insert into Person(Email) values('a@b.com'),('c@d.com'),('a@b.com')
select Email from Person

select  distinct l1.Email from Person l1, Person l2 where l1.Email=l2.Email and l1.Id!=l2.Id