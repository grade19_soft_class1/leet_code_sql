Create table course
(
Id int primary key identity(1,1) not null,
student varchar(255) not null,
class varchar(255) not null,
)

insert into course(student,class) values('A','Math'),('B','English'),('C','Math'),('D','Biology'),('E','Math'),('F','Computer'),('G','Math'),('H','Math'),('I','Math')

select class from course
select class from course group by class having count(distinct(student)) >= 5