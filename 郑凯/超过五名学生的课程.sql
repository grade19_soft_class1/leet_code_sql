create table courses (Student  varchar(255) ,class varchar(255))
insert into courses (Student,class) values ('A','Math')
insert into courses (Student,class) values ('B','English')
insert into courses (Student,class) values ('C','Math')
insert into courses (Student,class) values ('D','Biology')
insert into courses (Student,class) values ('E','Math')
insert into courses (Student,class) values ('F','Computer')
insert into courses (Student,class) values ('G','Math')
insert into courses (Student,class) values ('H','Math')
insert into courses (Student,class) values ('I','Math')

select class
from courses
group by class 

having count (distinct Student)>=5;

drop table courses
