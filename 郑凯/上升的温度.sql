Create table  Weather (Id int, RecordDate date, Temperature int)
Truncate table Weather
insert into Weather (Id, RecordDate, Temperature) values ('1', '2015-01-01', '10')
insert into Weather (Id, RecordDate, Temperature) values ('2', '2015-01-02', '25')
insert into Weather (Id, RecordDate, Temperature) values ('3', '2015-01-03', '20')
insert into Weather (Id, RecordDate, Temperature) values ('4', '2015-01-04', '30')
Select W1.Id
From Weather As W1,Weather As W2
Where W1.RecordDate = Dateadd(Day,1,W2.RecordDate)
And W1.Temperature > W2.Temperature

drop table Weather