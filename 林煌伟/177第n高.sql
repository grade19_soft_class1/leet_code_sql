﻿/*编写一个 SQL 查询，获取 Employee 表中第 n 高的薪水（Salary）。

+----+--------+
| Id | Salary |
+----+--------+
| 1  | 100    |
| 2  | 200    |
| 3  | 300    |
+----+--------+
例如上述 Employee 表，n = 2 时，应返回第二高的薪水 200。如果不存在第 n 高的薪水，那么查询应返回 null。

+------------------------+
| getNthHighestSalary(2) |
+------------------------+
| 200                    |
+------------------------+

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/nth-highest-salary
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。*/

use Leetcode 

create table Employee (
	ID int primary key not null identity ,
	Salary  int 
)

insert into Employee (Salary) values (100),(200),(300)

select * from Employee 

declare @Hight  int =2

select s1.salary
      from Employee s1,Employee s2
      where s2.salary>=s1.salary
      group by s1.salary
      having count(s2.salary)=@Hight

set @Hight =3
 
select s1.salary
      from Employee s1,Employee s2
      where s2.salary>=s1.salary
      group by s1.salary
      having count(s2.salary)=@Hight

select a.Salary from Employee a , Employee b
where b.Salary >=a.Salary 
group by a.Salary 
having count (b.Salary)=3
