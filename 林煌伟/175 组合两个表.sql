use Leetcode
create table Person 
(
	PersonID int not null primary key identity ,
	FirstName varchar (80) ,
	LastName varchar (80) ,
)

create table Address 
(
	AddressID int not null primary key identity ,
	PersonID int ,
	City varchar (80) ,
	State varchar (80) ,
)

select a.City,a.State,b.FirstName,b.LastName  from Address a left join Person b 
on a.PersonID=b.PersonID



