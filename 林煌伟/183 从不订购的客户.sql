create table Customers (
	ID int not null primary key identity ,
	Name varchar (80) ,
)

create table Orders (
	ID int primary key not null identity ,
	CustomersID int ,
)

select * from Customers

insert into Customers (Name) values ('Joe'),('Henry'),('Sam'),('Max')

select * from Orders

insert into Orders (CustomersID) values (3),(1)

select a.Name from Customers a left join Orders b
on a.ID=b.CustomersID 
where b.CustomersID is null 