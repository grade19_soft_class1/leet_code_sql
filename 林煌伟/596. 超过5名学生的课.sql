use Leetcode 

create table Course (
	ID int not null primary key identity ,
	Student varchar (80) not null,
	Class varchar (80) not null,
)

insert into Course (Student,Class) values ('A','Math'),('B','English'),('C','Math'),('D','Biology'),('E','Math'),('F','Computer'),('G','Math'),('H','Math'),('I','Math')

select Class from Course 
group by Class 
having count(Class)>1

