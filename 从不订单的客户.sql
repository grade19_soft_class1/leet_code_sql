Create table Customers
(Id int primary key identity(1,1) not null,
Name varchar(255) not null,
)
Create table Orders 
(
Id int primary key identity(1,1) not null,
CustomerId  int not null  references Customers(Id) ,
)

insert into Customers(Name) values('joe'),('Henry'),('Sam'),('Max')
insert into Orders(CustomerId) values(3),(1)

select Name from Customers

select Name as Customers from Customers a left join Orders b  on b.CustomerId=a.Id   where b.CustomerId Is Null 