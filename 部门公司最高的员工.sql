Create table Employee 
(
Id int primary key identity(1,1) not null,
Name varchar(255) not null,
Salary int not null,
DepartmentId  int not null,
)

Create table Department
(
Id int primary key identity(1,1) not null,
Name varchar(255) not null,
 )

 insert into Employee ( Name, Salary, DepartmentId) values ( 'Joe', 700, 1)
insert into Employee ( Name, Salary, DepartmentId) values ( 'Henry', 800, 2)
insert into Employee ( Name, Salary, DepartmentId) values ( 'Sam',600,2)
insert into Employee ( Name, Salary, DepartmentId) values ( 'Max', 900, 1)
insert into Department ( Name) values ( 'IT')
insert into Department (Name) values ( 'Sales')

select b.Name as Department ,a.Name as Employee ,a.Salary from Department b  full join 
Employee as a on a.DepartmentId = b.Id   where a.Salary=(select max(Salary) from Employee where DepartmentId = b.Id)

