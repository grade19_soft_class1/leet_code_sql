create table banji
(
	banjiID int primary key identity(1,1),
	banjiName nvarchar(80) not null,
	banjiNum int not null,
)
create table jiaoshi
(
	jiaoshiId int primary key identity(1,1),
	nanjiName nvarchar(80) not null,
)
create table banji_jiaoshi_mapping
(
	banjiID int foreign key(banjiID) references banji(banjiID),
	jiaoshiID int foreign key(jiaoshiID)references jiaoshi(jiaoshiID),
	kecheng nvarchar(20),
	constraint Pk_banji_ID_jiaoshi_ID primary key(banjiID,jiaoshiID)
)