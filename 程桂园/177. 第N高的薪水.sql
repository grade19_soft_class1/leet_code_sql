﻿/*编写一个 SQL 查询，获取 Employee 表中第 n 高的薪水（Salary）。

+----+--------+
| Id | Salary |
+----+--------+
| 1  | 100    |
| 2  | 200    |
| 3  | 300    |
+----+--------+
例如上述 Employee 表，n = 2 时，应返回第二高的薪水 200。如果不存在第 n 高的薪水，那么查询应返回 null。

+------------------------+
| getNthHighestSalary(2) |
+------------------------+
| 200                    |
+------------------------+
*/


create function getNthHighestSalary(@n int) returns int as
begin
		set @n =@n - 1;
		if @n < 0
			return null;
		declare @intreturn as int;
		select @intreturn = max(salary)
		from employee
		where salary not in (select distinct top (@n) salary from employee order by salary desc);
		return @intreturn; 
end


