create table Persons 
(
	Id int not null,
	Email varchar(30) not null,
)

insert Persons values('1','a@b.com')
insert Persons values('2','c@d.com')
insert Persons values('3','a@b.com')

select * from Persons

select Email
from Persons
group by Email having count(*)>1