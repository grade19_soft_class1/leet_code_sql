create table	Employee_fours
(
	Id int primary key not null,
	Name varchar(20) not null,
	Salary int not null,
	DepartmentId int not null,
)

create table Departmentss
(
	Id int not null references Employee_fours(Id),
	Name varchar(20) not null,
)
insert Employee_fours values('1','Joe','85000','1')
insert Employee_fours values('2','Henry','80000','2')
insert Employee_fours values('3','Sam','60000','2')
insert Employee_fours values('4','Max','90000','1')
insert Employee_fours values('5','Janet','69000','1')
insert Employee_fours values('6','Randy','85000','1')
insert Employee_fours values('7','Will ','70000','1')

insert Departmentss values('1','IT')
insert Departmentss values('2','Sales')

select tt.Departmentss,tt.Name employee,tt.salary
from
(select e.*,d.Name as Departmentss,dense_rank() over(partition by e.DepartmentId order by salary desc) as RN
from Employee_fours e,Departmentss d
where e.DepartmentId=d.id) AS tt
where tt.RN<=3
order by Departmentss asc,salary desc;

