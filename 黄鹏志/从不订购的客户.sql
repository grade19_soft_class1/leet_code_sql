create table Customers
(
	Id int primary key,
	Name varchar(20) not null,
)
create table Orders
(
	Id int not null,
	CustomerId int not null references Customers(Id),
)

insert Customers values('1','Joe')
insert Customers values('2','Henry')
insert Customers values('3','Sam')
insert Customers values('4','Max')

insert Orders values('1','3')
insert Orders values('2','1')

select * from Customers,Orders where Customers.Id = Orders.CustomerId
select * from Orders where CustomerId <>Id