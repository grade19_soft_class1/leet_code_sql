Create table Scores (Id int, Score DECIMAL(3,2))


insert into Scores (Id, Score) values ('1', '3.511')
insert into Scores (Id, Score) values ('2', '3.65')
insert into Scores (Id, Score) values ('3', '4.0')
insert into Scores (Id, Score) values ('4', '3.85')
insert into Scores (Id, Score) values ('5', '4.0')
insert into Scores (Id, Score) values ('6', '3.65')


SELECT * FROM Scores 

----第一部分 对分数进行降序排序

SELECT A.SCORE AS SCORE FROM SCORES A
ORDER BY A.SCORE DESC

第二部分

select b.Score from Scores b where b.Score >= X   
select count(distinct b.Score) from Scores b where b.Score >= X as Rank

select a.Score as Score,
(select count(distinct b.Score) from Scores b where b.Score >= a.Score) as Rank
from Scores a
order by a.Score DESC



-----窗口函数  涉及到排名的问题，都可以使用窗口函数来解决。记住rank, dense_rank, row_number排名的区别。

select *,             
   rank() over (order by score desc) as ranking,
   dense_rank() over (order by score desc) as dese_rank,
   row_number() over (order by score desc) as row_num
from Scores